// QUERY OPERATORS

// $gt / gte operator
/*
- Allows us to have documents that have field number values greater than
or equal to specifid value.
- Syntax: 
	db.collectionName.find({field: {$gt: value}})
	db.collectionName.find({field: {$gte: value}})
*/
db.users.find({age:{$gt:65}});
db.users.find({age:{$gte:65}});


// $lt / $lte operator

/*
- Allows us to have documents that have field number less than or equal to specifid value.
- Syntax: 
	db.collectionName.find({field: {$lt: value}})
	db.collectionName.find({field: {$lte: value}})
*/
db.users.find({age:{$lt: 76}});
db.users.find({age:{$lte: 65}});


// $ne operator

/*
- Allows us to have documents that have field number that are not equal to a specifid value.
- Syntax: 
	db.collectionName.find({field: {$lt: value}})
	db.collectionName.find({field: {$lte: value}})
*/
db.users.find({age:{$ne: 82}});

// LOGICAL QUERY OPERATORS

// $or
/*
- Allows us to find documents that match a single criteria from multiple search criteria provided
Syntax:
	db.collectionName.find({$or: [{"fieldA: valueA"}, {"fieldB: valueB"}]})
*/
db.users.find({$or: [{firstName: "Neil"},{firstName: "Bill"}]});
db.users.find({$or: [{firstName: "Neil"}, {firstName: "Stephen"}]});
db.users.find({$or: [{firstName: "Neil"}, {firstName: "Bill"}, {firstName: "Jane"}]});
db.users.find({$or: [{firstName:"Neil"}, {age:{$gt: 30 }}]});

//$and operator
/*
- Allows us to find documents matching multiple criteria in a single field
Syntax:
	db.collectionName.find({$and: 
	[{fieldA: valueA}, {fieldB: valueB}]
	})
*/
db.users.find({$and: 
	[{age:{$ne:82}}, {age:{$ne:76}}]
});

//db.users.find({$and:[{age:{$lt:65}}, {age:{$gt:65}}]}); // fetch 0


// FIELD PROJECTION
